/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

/**
 *
 * @author jeffr
 */
public class FNE {

    int plazo;
    double cuota, aux, abono;
    double vCuota[], vAbono[], vSaldo[];
    double vInteres[];

    public static double VPN(double fne[], double inversion, double tasa, int vu) {
        double vna = 0;

        for (int i = 1; i < vu + 1; i++) {
           
            vna +=  fne[i] / Math.pow((1 + tasa), i)-inversion;
        }

        return vna;
    }

    public static double[] arrayUtilidadAntesDeImp(double b[], double c[], double d[], double edif[], double in[], int vu) {

        double UAI[] = null;
        UAI = new double[vu + 1];

        for (int i = 0; i < vu + 1; i++) {
            UAI[i] = b[i] - c[i] - d[i]-edif[i] - in[i];
        }

        return UAI;
    }

    public static double[] arrayIR(double a[], int vu) {
        double ir[] = null;
        ir = new double[vu + 1];
        double ire = 0.30;
        for (int i = 0; i < vu + 1; i++) {
            ir[i] = a[i] * ire;
        }
        return ir;
    }

//    public static Object[] prestamo(double prestamo) {
//        Object p[] = new Object[1];
//
//        p[0] = prestamo;
//
//        return p;
//    }

    public static double[] FlujoNetoEfectivo(double[] udi, double[] dep,double dep2[], double[] vr, double capital[],
            double[] pres,
            double[] inv, double[] abono, int vu) {

        double fne[] = new double[vu + 1];
        for (int i = 0; i < vu + 1; i++) {

//            if(i==0){
//            fne[i]=0;
//            }
            fne[i] = udi[i] + dep[i]+dep2[i] + vr[i]+ capital[i] + pres[i] - inv[i] - abono[i];

        }

        return fne;
    }
    
     public static double[] FlujoNetoEfectivoPOs0(double[] pres,
            double[] inv, int vu) {

        double fne[] = new double[vu + 1];
        for (int i = 0; i < vu + 1; i++) {
           fne[i]= pres[i]-inv[i];
        }

        return fne;
    }

//    public static Object[] inversion(double inversion, int vu) {
//        Object p[] = new Object[vu];
//        for (int i = 0; i < vu; i++) {
//if(i==0){
// p[0] = inversion;
//
//}
//            }
//
//        return p;
//    }
    public static double[] arrayUDI(double a[], double b[], int vu) {
        double ir[] = null;
        ir = new double[vu + 1];

        for (int i = 0; i < vu + 1; i++) {
            ir[i] = a[i] - b[i];
        }
        return ir;
    }

    public static double[] suma_digitos(double valorAc, double vs, int vu) {
        double sdai[] = null;

        if (vu == 0) {
            return sdai;
        }
        if ((valorAc - vs) == 0) {
            return sdai;
        }

        sdai = new double[vu + 1];
        for (int i = 0; i < vu + 1; i++) {
            sdai[0] = 0;
            sdai[i] = (double) 2 * (vu - i + 1) / (double) (vu * (vu + 1)) * (valorAc - vs);
            

        }

        return sdai;
    }

//    public double[] ProporcionalInteres(double prestamo, double tasa, int plazo, int tasaInters) {
//        vSaldo = new double[plazo + 1];
//        vAbono = new double[plazo + 1];
//        vInteres = new double[plazo + 1];
//        vCuota = new double[plazo + 1];
//        abono = prestamo / plazo;
//        vSaldo[0] = prestamo;
//        aux = 0;
//
//        for (int i = 0; i < plazo + 1; i++) {
//
//            if (i != 0) {
//                vInteres[0] = 0;
//                vAbono[i] = abono;
//                vInteres[i] = tasa * vSaldo[i - 1];
//                 vCuota[i] = vAbono[i] + vInteres[i];
//
//                vSaldo[i] = vSaldo[i - 1] - vAbono[i];
//            }
//
////            if (i == plazo) {
////                aux = vSaldo[i];
////                vSaldo[i] = 0;
////            }
//
//        }
//
//        return vInteres;
//    }

    public double[] ProporcionalInteres(double prestamo, double tasa, int plazo, int tasaInt) {
        vSaldo = new double[plazo + 1];
        vAbono = new double[plazo + 1];
        vInteres = new double[plazo + 1];
        vCuota = new double[plazo + 1];
        abono = prestamo / plazo;
        vSaldo[0] = prestamo;
        aux = 0;

        for (int i = 0; i < plazo + 1; i++) {
            

            if (i != 0 && i<=tasaInt) {
                vInteres[0] = 0;
                vAbono[i] = abono;
                vInteres[i] = tasa * vSaldo[i - 1];
                 vCuota[i] = vAbono[i] + vInteres[i];

                vSaldo[i] = vSaldo[i - 1] - vAbono[i];
            }else{
            vInteres[i]=0;
            }

//            if (i == plazo) {
//                aux = vSaldo[i];
//                vSaldo[i] = 0;
//            }

        }

        return vInteres;
    }
    
    public double[] ProporcionalAbono(double prestamo, double tasa, int plazo) {
        vSaldo = new double[plazo + 1];
        vAbono = new double[plazo + 1];
        vInteres = new double[plazo + 1];
        vCuota = new double[plazo + 1];
        abono = prestamo / plazo;
        vSaldo[0] = prestamo;
        aux = 0;
        vAbono[0] = 0;
        for (int i = 0; i < plazo + 1; i++) {

            if (i != 0) {
                vAbono[i] = abono;
                vInteres[i] = tasa * vSaldo[i - 1];
                vCuota[i] = vAbono[i] + vInteres[i];

                vSaldo[i] = vSaldo[i - 1] - vAbono[i];
            }

            if (i == plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }

        }

        return vAbono;
    }

}
