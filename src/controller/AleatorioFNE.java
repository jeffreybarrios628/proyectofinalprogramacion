/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import javax.swing.JOptionPane;

/**
 *
 * @author jeffr
 */
public class AleatorioFNE {

    private RandomAccessFile raf;
    private File file;
    private String filename;

    private void openRAF() throws FileNotFoundException, IOException {
        filename = "FNE.txt";
        file = new File(filename);
        if (!file.exists()) {
            raf = new RandomAccessFile(file, "rw");
//            raf.seek(0);
//            raf.writeInt(0);
//            raf.writeInt(0);
        } else {
            raf = new RandomAccessFile(file, "rw");
        }
    }

    private void close() throws IOException {
        if (raf != null) {
            raf.close();
        }
    }

    

//    public DatosFNE findById(int id) throws IOException {
//        openRAF();
//        raf.seek(0);
//        int n = raf.readInt();
//        if (n == 0) {
//            return null;
//        }
////        int index = runBinarySearchRecursively(id, 0, n - 1);
////        if (index <= -1) {
////            return null;
////        }
//        DatosFNE fne = new DatosFNE();
//        long pos =  8+ id * 650;
//        raf.seek(pos);
//        fne.setN(raf.readInt());
//        fne.setInversion(raf.readUTF());
//        fne.setVs(raf.readUTF());
//        fne.setVu(raf.readUTF());
//        fne.setIngresos_anuales(raf.readUTF());
//        fne.setCrecimiento_ingresos(raf.readUTF());
//        fne.setCostos(raf.readUTF());
//        fne.setCrecimiento_costos(raf.readUTF());
//        fne.setPrestamo(raf.readUTF());
//        
//        fne.setTasa_interes(raf.readUTF());
//        fne.setPlazo_prest(raf.readUTF());
//        fne.setTmar(raf.readUTF());
//
//        close();
//        return fne;
//    }
    
public DatosFNE abrirArchivo(File archivo) throws FileNotFoundException, IOException {

        DatosFNE fne = new DatosFNE();
        RandomAccessFile raf = new RandomAccessFile(archivo, "rw");
       
        
        fne.setEquipo(raf.readUTF());
        fne.setEdificio(raf.readUTF());
        fne.setCapital(raf.readUTF());
        fne.setVs(raf.readUTF());
        fne.setVu(raf.readUTF());
        fne.setIngresos_anuales(raf.readUTF());
        fne.setCrecimiento_ingresos(raf.readUTF());
        fne.setCostos(raf.readUTF());
        fne.setCrecimiento_costos(raf.readUTF());
        fne.setPrestamo(raf.readUTF());
        
        fne.setTasa_interes(raf.readUTF());
        fne.setPlazo_prest(raf.readUTF());
        fne.setTmar(raf.readUTF());
        return fne;

    }
    public void save(File archivo,String equipo, String edificio, String capital, String vs,String vu,String ingresos, 
            String CrecIngresos, String costos,
            String crecCostos, String prestamo,String tasaInteres,String plazoInteres,String tmar) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(archivo, "rw");
        
   
      
        raf.writeUTF(equipo);
         raf.writeUTF(edificio);
        raf.writeUTF(capital);
        raf.writeUTF(vs);
        raf.writeUTF(vu);
        raf.writeUTF(ingresos);
        raf.writeUTF(CrecIngresos);
        raf.writeUTF(costos);
        raf.writeUTF(crecCostos);
        raf.writeUTF(prestamo);
        raf.writeUTF(tasaInteres);
        raf.writeUTF(plazoInteres);
        raf.writeUTF(tmar);

        close();
    }

}
