/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author jeffr
 */
public class TablaDePagoController {
    
    int plazo;
    double cuota,aux,abono;
    double vCuota[],vInteres[],vAbono[], vSaldo[];
    
     public  double []  NiveladaCuota(double prestamo,double tasa, int plazo){
         
         
      vSaldo= new double[plazo+1];
      vAbono= new double[plazo+1];
      vInteres= new double[plazo+1];
      vCuota= new double[plazo+1];
        cuota = prestamo*((tasa*Math.pow(1+tasa, plazo))/(Math.pow(1+tasa, plazo)-1));
        vSaldo[0]=prestamo;
        vCuota[0]=0.00;
        aux=0;
        
        for (int i = 0; i < plazo+1; i++) 
        {
            
            if (i!=0) {
                vCuota[i]=cuota;
                vInteres[i]=tasa*vSaldo[i-1];
                vAbono[i]=vCuota[i]-vInteres[i];
                vSaldo[i]=vSaldo[i-1]-vAbono[i];
            }
            
            if (i==plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }
        }
        
        return vCuota;
     }
     public  double []  NiveladaSaldo(double prestamo,double tasa, int plazo){
         
         
      vSaldo= new double[plazo+1];
      vAbono= new double[plazo+1];
      vInteres= new double[plazo+1];
      vCuota= new double[plazo+1];
        cuota = prestamo*((tasa*Math.pow(1+tasa, plazo))/(Math.pow(1+tasa, plazo)-1));
        vSaldo[0]=prestamo;
        vCuota[0]=0.00;
        aux=0;
        
        for (int i = 0; i < plazo+1; i++) 
        {
            
            if (i!=0) {
                vCuota[i]=cuota;
                vInteres[i]=tasa*vSaldo[i-1];
                vAbono[i]=vCuota[i]-vInteres[i];
                vSaldo[i]=vSaldo[i-1]-vAbono[i];
            }
            
            if (i==plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }
        }
        
        return vSaldo;
     }
     
     public  double []  NiveladaAbono(double prestamo,double tasa, int plazo){
         
         
      vSaldo= new double[plazo+1];
      vAbono= new double[plazo+1];
      vInteres= new double[plazo+1];
      vCuota= new double[plazo+1];
        cuota = prestamo*((tasa*Math.pow(1+tasa, plazo))/(Math.pow(1+tasa, plazo)-1));
        vSaldo[0]=prestamo;
        vCuota[0]=0.00;
        aux=0;
        
        for (int i = 0; i < plazo+1; i++) 
        {
            
            if (i!=0) {
                vCuota[i]=cuota;
                vInteres[i]=tasa*vSaldo[i-1];
                vAbono[i]=vCuota[i]-vInteres[i];
                vSaldo[i]=vSaldo[i-1]-vAbono[i];
            }
            
            if (i==plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }
        }
        
        return vAbono;
     }
     
     
     
     
public  double []  NiveladaInteres(double prestamo,double tasa, int plazo){
         
         
      vSaldo= new double[plazo+1];
      vAbono= new double[plazo+1];
      vInteres= new double[plazo+1];
      vCuota= new double[plazo+1];
        cuota = prestamo*((tasa*Math.pow(1+tasa, plazo))/(Math.pow(1+tasa, plazo)-1));
        vSaldo[0]=prestamo;
        vCuota[0]=0.00;
        aux=0;
        
        for (int i = 0; i < plazo+1; i++) 
        {
            
            if (i!=0) {
                vCuota[i]=cuota;
                vInteres[i]=tasa*vSaldo[i-1];
                vAbono[i]=vCuota[i]-vInteres[i];
                vSaldo[i]=vSaldo[i-1]-vAbono[i];
            }
            
            if (i==plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }
        }
        
        return vInteres;
     }


  public double[] ProporcionalAbono(double prestamo,double tasa, int plazo){
         vSaldo= new double[plazo+1];
      vAbono= new double[plazo+1];
      vInteres= new double[plazo+1];
      vCuota= new double[plazo+1];
        abono = prestamo/plazo;
        vSaldo[0]=prestamo;
        aux=0;
        
        for (int i = 0; i < plazo+1; i++) {
            
            if (i!=0) {
                vAbono[i]=abono;
                vInteres[i]=tasa*vSaldo[i-1];
                vCuota[i]=vAbono[i]+vInteres[i];
                
                vSaldo[i]=vSaldo[i-1]-vAbono[i];
            }
            
            if (i==plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }
        
    }
    
return vAbono;
  }
  
  
   public double[] ProporcionalInteres (double prestamo,double tasa, int plazo){
         vSaldo= new double[plazo+1];
      vAbono= new double[plazo+1];
      vInteres= new double[plazo+1];
      vCuota= new double[plazo+1];
        abono = prestamo/plazo;
        vSaldo[0]=prestamo;
        aux=0;
        
        for (int i = 0; i < plazo+1; i++) {
            
            if (i!=0) {
                vAbono[i]=abono;
                vInteres[i]=tasa*vSaldo[i-1];
                vCuota[i]=vAbono[i]+vInteres[i];
                
                vSaldo[i]=vSaldo[i-1]-vAbono[i];
            }
            
            if (i==plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }
        
    }
    
return vInteres;
  }
   
    public double[] ProporcionalCuota(double prestamo,double tasa, int plazo){
         vSaldo= new double[plazo+1];
      vAbono= new double[plazo+1];
      vInteres= new double[plazo+1];
      vCuota= new double[plazo+1];
        abono = prestamo/plazo;
        vSaldo[0]=prestamo;
        aux=0;
        
        for (int i = 0; i < plazo+1; i++) {
            
            if (i!=0) {
                vAbono[i]=abono;
                vInteres[i]=tasa*vSaldo[i-1];
                vCuota[i]=vAbono[i]+vInteres[i];
                
                vSaldo[i]=vSaldo[i-1]-vAbono[i];
            }
            
            if (i==plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }
        
    }
    
return vCuota;
  }
    
     public double[] ProporcionalSaldo(double prestamo,double tasa, int plazo){
         vSaldo= new double[plazo+1];
      vAbono= new double[plazo+1];
      vInteres= new double[plazo+1];
      vCuota= new double[plazo+1];
        abono = prestamo/plazo;
        vSaldo[0]=prestamo;
        aux=0;
        
        for (int i = 0; i < plazo+1; i++) {
            
            if (i!=0) {
                vAbono[i]=abono;
                vInteres[i]=tasa*vSaldo[i-1];
                vCuota[i]=vAbono[i]+vInteres[i];
                
                vSaldo[i]=vSaldo[i-1]-vAbono[i];
            }
            
            if (i==plazo) {
                aux = vSaldo[i];
                vSaldo[i] = 0;
            }
        
    }
    
return vSaldo;
  }
}


