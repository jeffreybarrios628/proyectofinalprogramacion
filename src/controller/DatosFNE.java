/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author jeffr
 */
public class DatosFNE {
 
   // public String inversion;
   public String Equipo;
   public String Edificio;
    public String capital;
    public String vs;
    public String vu;
    public String ingresos_anuales;
    public String crecimiento_ingresos;
    public String costos;
    public String crecimiento_costos;
    public String prestamo;
    public String tasa_interes;
    public String plazo_prest;
    public String tmar;

    public DatosFNE(String Equipo, String Edificio, String capital, String vs, String vu, String ingresos_anuales, String crecimiento_ingresos, String costos, String crecimiento_costos, String prestamo, String tasa_interes, String plazo_prest, String tmar) {
        this.Equipo = Equipo;
        this.Edificio = Edificio;
        this.capital = capital;
        this.vs = vs;
        this.vu = vu;
        this.ingresos_anuales = ingresos_anuales;
        this.crecimiento_ingresos = crecimiento_ingresos;
        this.costos = costos;
        this.crecimiento_costos = crecimiento_costos;
        this.prestamo = prestamo;
        this.tasa_interes = tasa_interes;
        this.plazo_prest = plazo_prest;
        this.tmar = tmar;
    }
public DatosFNE(){}

    public String getEquipo() {
        return Equipo;
    }

    public void setEquipo(String Equipo) {
        this.Equipo = Equipo;
    }

    public String getEdificio() {
        return Edificio;
    }

    public void setEdificio(String Edificio) {
        this.Edificio = Edificio;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getVs() {
        return vs;
    }

    public void setVs(String vs) {
        this.vs = vs;
    }

    public String getVu() {
        return vu;
    }

    public void setVu(String vu) {
        this.vu = vu;
    }

    public String getIngresos_anuales() {
        return ingresos_anuales;
    }

    public void setIngresos_anuales(String ingresos_anuales) {
        this.ingresos_anuales = ingresos_anuales;
    }

    public String getCrecimiento_ingresos() {
        return crecimiento_ingresos;
    }

    public void setCrecimiento_ingresos(String crecimiento_ingresos) {
        this.crecimiento_ingresos = crecimiento_ingresos;
    }

    public String getCostos() {
        return costos;
    }

    public void setCostos(String costos) {
        this.costos = costos;
    }

    public String getCrecimiento_costos() {
        return crecimiento_costos;
    }

    public void setCrecimiento_costos(String crecimiento_costos) {
        this.crecimiento_costos = crecimiento_costos;
    }

    public String getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(String prestamo) {
        this.prestamo = prestamo;
    }

    public String getTasa_interes() {
        return tasa_interes;
    }

    public void setTasa_interes(String tasa_interes) {
        this.tasa_interes = tasa_interes;
    }

    public String getPlazo_prest() {
        return plazo_prest;
    }

    public void setPlazo_prest(String plazo_prest) {
        this.plazo_prest = plazo_prest;
    }

    public String getTmar() {
        return tmar;
    }

    public void setTmar(String tmar) {
        this.tmar = tmar;
    }

    
   
    
    
}
