/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author jeffr
 */
public class InteresCompuestoController {
    
     double x;
    public double presentToFuturo(double p, double i, double n)
    {
        x=p*(Math.pow((1+i),n));
        return x;
    }
    public double futureToPresent(double f, double i, double n)
    {
        x=f/(Math.pow((1+i),n));
        return x;
    }
    public double Tasa(double vp,double vf,double plazo)
    {
        x=(Math.pow((vf/vp),(1/plazo)))-1;
        return x;
    }
    public double Plazo(double vp,double vf,double i)
    {
        x=Math.log(vf/vp)/Math.log(1+i);
        return x;
    }
    
    
}
